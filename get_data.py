from TwitterAPI import TwitterAPI


api = TwitterAPI(
    "xxx",
    "xxx",
    "xxx",
    "xxx")

r = api.request('search/tweets', {'q': 'twilight', 'lang': 'en', 'count': 100})

for item in r:
    with open("data/"+str(item['id'])+".tweet", "w") as f:
        f.write(item['text'].encode('UTF-8'))
