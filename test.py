from nltk import pos_tag
from nltk.tokenize import word_tokenize
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews
from nltk.corpus import PlaintextCorpusReader


def word_feats(text):
    words = word_tokenize(text)
    words = pos_tag(words)
    return dict(words)

negids = movie_reviews.fileids('neg')
posids = movie_reviews.fileids('pos')

negfeats = [(word_feats(movie_reviews.raw(fileids=[f])), 'neg')
            for f in negids]
posfeats = [(word_feats(movie_reviews.raw(fileids=[f])), 'pos')
            for f in posids]

trainfeats = negfeats + posfeats

classifier = NaiveBayesClassifier.train(trainfeats)

classifier.show_most_informative_features()

print 'classify tweets'

corpus_root = 'data'
tweetlist = PlaintextCorpusReader(corpus_root, '.*\.tweet')

for tweet in tweetlist.fileids():
    what = classifier.classify(word_feats(tweetlist.raw(tweet)))
    print "%s = %s" % (tweet, what)
    print "\t%s" % (tweetlist.raw(tweet).encode('utf8'))
    print "=============================="
